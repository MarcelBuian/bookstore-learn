<?php

namespace Tests\Feature;

use App\Author;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AuthorsTest extends TestCase
{
    use DatabaseTransactions;

    public function testUnAutorPoateFiCreat()
    {
        // Given there is a user.
        $user = factory(User::class)->create();

        // And he is authenticated.
        $this->actingAs($user);

    	// When he makes a post with:
        $response = $this->post('/authors/create', [
            'first_name' => 'Ion',
            'last_name' => 'Creanga',
        ]);

        // 1. Then we expect in database having this author.
        $this->assertDatabaseHas('authors', [
            'first_name' => 'Ion',
            'last_name' => 'Creanga',
        ]);

        // 2. And we expect to redirect browser to list of all authors.
        $response->assertRedirect('/authors');

        // 3. And we expect to this redirect page having an session with status about this.
        $response->assertSessionHas('status', 'Author Ion Creanga a fost creat.');
    }

    public function test_user_should_be_auth_before_creating()
    {
        // When we make a post with:
        $response = $this->post('/authors/create', [
            'first_name' => 'Ion',
            'last_name' => 'Creanga',
        ]);

        $response->assertRedirect('/login');

        // 1. Then we expect in database having this author.
        $this->assertDatabaseMissing('authors', [
            'first_name' => 'Ion',
            'last_name' => 'Creanga',
        ]);
    }

    public function test_user_can_see_an_author_even_if_is_not_logged()
    {
        $author = Author::forceCreate([
            'first_name' => 'Mihai',
            'last_name' => 'Eminescu',
        ]);

        $response = $this->get('/authors/'.$author->id);

        $response->assertStatus(200);
    }

    public function test_author_can_be_deleted()
    {
        // Given there is a user.
        $user = factory(User::class)->create();

        // And he is authenticated.
        $this->actingAs($user);
        
        $author = Author::forceCreate([
            'first_name' => 'Mihai',
            'last_name' => 'Eminescu',
        ]);

        $response = $this->delete('/authors/'.$author->id);

        // 2. And we expect to redirect browser to list of all authors.
        $response->assertRedirect('/authors');

        // 3. And we expect to this redirect page having an session with status about this.
        $response->assertSessionHas('status', 'Author Mihai Eminescu a fost sters.');

        // 4. Assert that we don't have this in DB.
        $this->assertDatabaseMissing('authors', [
            'first_name' => 'Mihai',
            'last_name' => 'Eminescu',
        ]);
    }
}
