<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class BookTest extends TestCase
{
    // https://laravel.com/docs/5.4/database-testing#resetting-the-database-after-each-test
    // Dupa fiecare test vrem ca sa renuntam(sa facem roll-back) la modificarile create/adaugate/sterse din baza de date.
    // Acesta este de fapt un Trait
    // http://php.net/manual/ro/language.oop5.traits.php
    use DatabaseTransactions;

    public function testExistaPaginaDeBooks()
    {
        $this->get('/books')
            ->assertStatus(200);
    }

    public function testNuExistaPaginaDeCarti()
    {
        // cele 2 abordari sunt egale.
        
        $status = $this->get('/carti')->getStatusCode();
        $this->assertFalse($status == 200);

        // $this->get('/carti')
        //     ->assertStatus(404);
    }

    public function test_un_utilizator_adauga_o_carte()
    {
        // Avem un "utilizatorul logat"
        $user = factory(User::class)->create();

        // Pregatim parametrii de input care se trimit in fisierul:
        // resources\views\books\create.blade.php
        $parameters = [
            'name' => 'Book test',
            'price' => 123,
            'content' => 'Continutul cartii bla-bla.',
            'pageNumber' => 100,
            'isOfficial' => '1'
        ];

        $this->actingAs($user)
            ->post('/books/store', $parameters)
            ->assertRedirect('/books')
        ;

        $this->assertDatabaseHas('books', [
            'name' => 'Book test',
            'price' => 123,
            'content' => 'Continutul cartii bla-bla.',
            'page_number' => 100,
            'official' => true,
            'added_by' => $user->id,
        ]);
    }

    public function test_utilizatorul_trebuie_sa_fie_logat_ca_sa_creeze_o_carte()
    {
        // Avem un utilizator, dar nu este logat
        $user = factory(User::class)->create();

        // Pregatim parametrii de input care se trimit in fisierul:
        // resources\views\books\create.blade.php
        $parameters = [
            'name' => 'Book test',
            'price' => 123,
            'content' => 'Continutul cartii bla-bla.',
            'pageNumber' => 100,
            'isOfficial' => '1'
        ];

        $this->post('/books/store', $parameters)
            ->assertRedirect('/login');

        $this->assertDatabaseMissing('books', [
            'name' => 'Book test',
        ]);
    }
}
