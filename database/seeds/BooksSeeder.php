<?php

use App\Book;
use App\User;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// argumentul 2 inseamna numarul de entitati create.
        // 10 carti create de un user nou.
        factory(Book::class, 10)->create();

        // 10 carti create de utilizatori existenti.
        for ($i=1; $i<= 10; $i++) {
            factory(Book::class, 1)->create([
                'added_by' => User::select('id')->get()->pluck('id')->random(),
            ]);
        }
    }
}
