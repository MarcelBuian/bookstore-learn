<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->increments('id');

            // presupunem ca nu ne intereseaza cand a fost creat sau modificat autorul.
            // Oricand nu dorim sa adaugam timestamps() (adica created_at si updated_at) trebuie sa le dezactivam din modelul in cauza.
            // $table->timestamps();
            $table->string('first_name', 32);
            $table->string('last_name', 32);

            // @TODO (tema de casa)
            // Adaugati mai multe campuri (cel putin 4 la numar).
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
