<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            // timestamps = created_at combinat cu updated_at
            $table->timestamps();

            // al doilea parametru (128) reprezinta numarul de caractere pe care il poate avea.
            $table->string('name', 128);

            // primul argument (6) inseamna numarul de cifre pe care il poate avea in total,
            // al doilea inseamna (2) numarul de decimale dupa virgula.
            // formatul poate fi (xxxx.xx)
            $table->float('price', 6, 2);

            // cu toate ca boolean in mysql nu exista, acesta ne lasa sa adaugam campul,
            // unde in realitate este echivalent cu 
            // $table->tinyInteger()
            // implicit va avea valoarea false (zero in baza de date)
            $table->boolean('official')->default(false);

            // unsigned (fara semn) poate incepe de la valoarea 0.
            $table->integer('page_number')->unsigned(); 

            // pentru tipul text, nu se poate asigna un numar limitat de caractere.
            $table->text('content');

            // doar campurile care au nullable() pot fi optionale. 
            $table->timestamp('printed_at')->nullable();

            // asta va fi user id
            $table->integer('added_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
