<?php

/**
 * Mai multe informatii gasiti aici:
 * 
 * https://github.com/fzaninotto/Faker
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Book::class, function (Faker\Generator $faker) {

	// primul argument (30) este posibilitatea de true in procente.
	$knowWhenIsPrinted = $faker->boolean(30);

    do {
        $name = $faker->word;
    } while (strlen($name) < 4);

    return [
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
        'name' => $faker->word,

        // primul argument = numarul maxim de decimale
        // al doilea argument = min
        // al treilea = max
        'price' => $faker->randomFloat(2, 0.01, 1000),

        'official' => $faker->boolean(),

        'page_number' => $faker->numberBetween(1, 200),

        'content' => $faker->paragraph(),

        // este echivalent cu:
        // if ($knowWhenIsPrinted == true) then $faker->dateTime; else null.
        'printed_at' => $knowWhenIsPrinted ? $faker->dateTime : null,

        'added_by' => factory(App\User::class)->create()->id,
    ];
});

    
$factory->define(App\Author::class, function (Faker\Generator $faker) {
    
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
    ];
});
