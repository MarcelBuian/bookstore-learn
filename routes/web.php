<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Lista cu rutele de auth se afla in vendor de la laravel in fisierul:
// \vendor\laravel\framework\src\Illuminate\Routing\Router.php la functia auth (994)
Auth::routes();

Route::get('/home', 'HomeController@index')->name('acasa');

Route::get('/books', 'BookController@index')->name('books.list');
Route::get('/books/create', 'BookController@create');
Route::post('/books/store', 'BookController@store')->name('creeaza-o-carte');
Route::get('/book/{book}', 'BookController@show');
Route::get('/books/{bookId}', 'BookController@redirectToShow');
Route::delete('/books/{book}', 'BookController@delete');
Route::get('/books/{book}/edit', 'BookController@edit');
Route::put('/books/{book}', 'BookController@update');


// Authors route:

Route::get('/authors', 'AuthorController@index');
Route::get('/authors/create', 'AuthorController@create');
Route::post('/authors/create', 'AuthorController@store');
Route::get('/authors/{author}', 'AuthorController@show');
Route::delete('/authors/{author}', 'AuthorController@delete');