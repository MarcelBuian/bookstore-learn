<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/books', 'Api\BookController@index');
Route::get('/books/{id}', 'Api\BookController@show');
Route::get('/book/{id}', 'Api\BookController@show');
// @TODO (completeaza post, put si delete pentru books)

Route::get('authors', 'Api\AuthorController@index');
Route::get('author/{author}', 'Api\AuthorController@show');
Route::post('author', 'Api\AuthorController@create');
Route::put('author/{idAuthor}', 'Api\AuthorController@update');
Route::delete('author/{idAuthor}', 'Api\AuthorController@delete');
