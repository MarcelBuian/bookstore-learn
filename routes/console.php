<?php

use App\Book;
use Illuminate\Foundation\Inspiring;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('db:copy', function () {

    $username = env('DB_USERNAME');
    $password = env('DB_PASSWORD');
    if ($password) {
        $password = ' -p'.$password;
    }
    $databaseName = env('DB_DATABASE');

    $resultFile = 'database/dump.sql';

    $mysqlDumpCommand = env('MYSQLDUMP_PATH', 'mysqldump');

    $command = $mysqlDumpCommand;
    $command .= ' -u'.$username.$password;
    $command .= ' '.$databaseName.' --result-file='.$resultFile;

    exec($command);

    $this->comment('Database succefully dumped into '.$resultFile);
})->describe('Make a dump of current database');

Artisan::command('db:recreate', function () {

    $env = env('APP_ENV');

    // Protectie, pentru a nu se rula pe serverul de productie.
    if ($env != 'local') {
        $this->error('Ce faci, ba Cristi?? Nu esti in local, esti in '.$env);
        return;
    }

    $databaseName = env('DB_DATABASE');

    $config = app(\Illuminate\Config\Repository::class);
    $config->set('database.connections.mysql.database', 'mysql');

    DB::statement("drop database if exists `$databaseName`");
    DB::statement("create database `$databaseName`");

    $this->comment("Database $databaseName was deleted (if exists) and empty recreated.");

    // Daca dorim sa mai facem o comanda mai jos, setam inapoi numele corect la baza de date din configuratii.
    $config->set('database.connections.mysql.database', $databaseName);
    // Actualizam baza de date.
    DB::reconnect();

    // Cream tabelul migrations (care in mod normal se creeaza automat, dar noi am cam fortat lucrurile)
    $this->call('migrate:install');

    // Aici se pot scrie orice alte operatii dorite.
    $this->call('migrate');

})->describe('Re-create empty database');