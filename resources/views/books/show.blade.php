@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">{{ $book->name }}</div>

                <div class="panel-body">
                    {{ $book->content }}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Price</div>
                <div class="panel-body">
                    {{ $book->price }}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Pages</div>
                <div class="panel-body">
                    {{ $book->page_number }}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Is Official</div>
                <div class="panel-body">
                    {{ $book->official ? 'Yes' : 'No' }}
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Content</div>
                <div class="panel-body">
                    {{ $book->content }}
                </div>
            </div>

            @if (Auth::check())
                <div class="panel panel-default">
                    
                    <form class="form-inline" method="POST" action="/books/{{ $book->id }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Delete this book</button>
                    </form>
                    <a href="/books/{{ $book->id }}/edit" class="btn btn-success form-inline">Editeaza</a>

                </div>
            @else
                <div class="alert alert-info" role="alert">
                    Pentru a edita sau sterge o carte,
                    <a href="/login" class="alert-link">logheaza-te</a>!
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
