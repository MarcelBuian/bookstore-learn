@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default col-md-6 col-md-offset-3">
            <div class="panel-body row">
                <div class="col-md-4">Books: {{ $books->total() }}</div>
                <div class="col-md-4 col-md-offset-4">
                    <a href="/books/create" class="btn btn-success">Create New</a>
                </div>
            </div>
            <div class="panel-body row">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($books->total() == 0)
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Nu este nicio carte! Adauga una.
                    </div>
                @endif
            </div>
        </div>
        
        <div class="col-md-8 col-md-offset-2">

            @foreach ($books as $book)

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/book/{{ $book->id }}">
                            {{ $book->name }}
                        </a>
                        creat in {{ $book->created_at }}
                        ( {{ $book->created_at->diffForHumans() }} )
                        ( {{ $book->addedBy->fullName }} )
                        <form class="form-inline" method="POST" action="/books/{{ $book->id }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">x</button>
                    </form>
                    </div>

                    <div class="panel-body">
                        {{ $book->content }}
                    </div>
                </div>

            @endforeach

            {{ $books->links() }}

        </div>
    </div>
</div>
@endsection
