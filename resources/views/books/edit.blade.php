@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    
                    <ol class="breadcrumb">
                      <li><a href="/home">Home</a></li>
                      <li><a href="/books">Carti</a></li>
                      <li><a href="/books/{{ $carte->id }}">{{ $carte->name }}</a></li>
                      <li class="active">Edit</li>
                    </ol>

                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/books/{{ $carte->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nume</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name', $carte->name) }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Pret</label>

                            <div class="col-md-6">
                                <input id="price" type="number" step="0.01" class="form-control" name="price" value="{{ old('price', $carte->price) }}" >

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Continut</label>

                            <div class="col-md-6">
                                <textarea id="content" class="form-control" name="content"
                                    rows="10"

                                >{{ old('content', $carte->content) }}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pageNumber') ? ' has-error' : '' }}">
                            <label for="pageNumber" class="col-md-4 control-label">Numarul de pagini</label>

                            <div class="col-md-6">
                                <input id="pageNumber" type="number" class="form-control" name="pageNumber" value="{{ old('pageNumber', $carte->numarulDePagini) }}" >

                                @if ($errors->has('pageNumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pageNumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input
                                    type="checkbox"
                                    name="isOfficial"
                                    value="1"
                                    {{ old('isOfficial', $carte->official) ? 'checked' : '' }}
                                > E oficiala treaba
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Modifica
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
