@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create new Book</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('creeaza-o-carte') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nume</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Pret</label>

                            <div class="col-md-6">
                                <input id="price" type="number" step="0.01" class="form-control" name="price" value="{{ old('price') }}" >

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Continut</label>

                            <div class="col-md-6">
                                <textarea id="content" class="form-control" name="content"
                                    rows="10"

                                >{{ old('content') }}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pageNumber') ? ' has-error' : '' }}">
                            <label for="pageNumber" class="col-md-4 control-label">Numarul de pagini</label>

                            <div class="col-md-6">
                                <input id="pageNumber" type="number" class="form-control" name="pageNumber" value="{{ old('pageNumber') }}" >

                                @if ($errors->has('pageNumber'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pageNumber') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input
                                    type="checkbox"
                                    name="isOfficial"
                                    value="1"
                                    {{ old('isOfficial') ? 'checked' : '' }}
                                > E oficiala treaba
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Salveaza
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
