@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">

            <div class="panel panel-default">
                <div class="panel-heading">{{ $author->first_name }}</div>

                <div class="panel-body">
                    {{ $author->last_name }}

                    <form class="form-inline" method="POST" action="/authors/{{ $author->id }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Delete this author</button>
                    </form>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
