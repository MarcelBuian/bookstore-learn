@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default col-md-6 col-md-offset-3">
            <div class="panel-body row">
                <div class="col-md-4">Authors : {{ $authors->total() }}</div>
                <div class="col-md-4 col-md-offset-4">
                    <a href="/authors/create" class="btn btn-success">Create New</a>
                </div>
            </div>
            <div class="panel-body row">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($authors->total() == 0)
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        There is no author! Create one.
                    </div>
                @endif
            </div>
        </div>
        
        <div class="col-md-8 col-md-offset-2">

            @foreach ($authors as $author)

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/authors/{{ $author->id }}">
                            {{ $author->first_name }} {{ $author->last_name }}
                        </a>
                    </form>
                    </div>

                    <div class="panel-body">
                        
                    </div>
                </div>

            @endforeach

            {{ $authors->links() }}

        </div>
    </div>
</div>
@endsection
