<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
	// Dezactivam folosirea timpestamps pentru acest model.
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
