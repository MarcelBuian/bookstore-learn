<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    private $rules = [
        'name' => 'required|string|max:50|min:4',
        'price' => 'required|numeric|min:0.50|max:9999',
        'content' => 'required|string|min:10|max:50000',
        'pageNumber' => 'required|integer|between:1,200',
        'isOfficial' => 'boolean'
    ];

    public function __construct()
    {
        // Acesta este un filtru cu numele de "auth"
        // care inseamna ca trebuie sa fii autentificat
        // in caz contrar esti redirectat catre pagina de login
        $this->middleware('auth')->except([
            'index',
            'show',
            'redirectToShow',
        ]);
    }

    // nu trebuie auth
    public function index()
    {
        // 10 e numarul de iteme per pagina. 
        // implicit cred ca e 20.
        $books = Book::with('addedBy')->orderBy('created_at', 'desc')
            ->paginate(10);

        // cele doua randuri sunt echivalente

        // return view('books', ['books' => $books]);
        return view('books.index', compact('books'));
    }

    // nu trebuie auth
    public function show(Book $book)
    {
        return view('books.show', ['book'=>$book]);
    }

    // nu trebuie auth
    public function redirectToShow($bookId)
    {
        return redirect('/book/'.$bookId);
    }

    // trebuie auth
    public function create()
    {
        return view('books.create');
    }

    // trebuie auth
    public function store(Request $request)
    {
        // isOfficial nu este required pentru ca daca nu este selectat atunci lipseste. (pentru ca e un checkbox) 
        $this->validate($request, $this->rules);

        $loggedUser = $request->user();

        $book = new Book;
        $book->name = $request->get('name');
        $book->price = $request->get('price');
        $book->content = $request->get('content');
        $book->page_number = $request->get('pageNumber');
        $book->official = $request->get('isOfficial', false);
        $book->added_by = $loggedUser ? $loggedUser->id : null;
        $book->save();

        return redirect('/books')->with('status', 'Book created!');
    }

    // trebuie auth
    public function delete(Book $book)
    {
        $book->delete();

        $message = 'Book '.$book->name.' was deleted!';
        
        return redirect('/books')->with('status', $message);
    }

    // trebuie auth
    public function edit(Book $book)
    {
        return view('books.edit', [
            'carte' => $book
        ]);
    }

    // trebuie auth
    public function update(Book $book, Request $request)
    {
        // isOfficial nu este required pentru ca daca nu este selectat atunci lipseste. (pentru ca e un checkbox) 
        $this->validate($request, $this->rules);

        $book->update([
            'name' => $request->get('name'),
            'price' => $request->get('price'),
            'content' => $request->get('content'),
            'page_number' => $request->get('pageNumber'),
            'official' => $request->get('isOfficial', false),
            'added_by' => $request->user()->id,
        ]);

        return redirect('/books')->with('status', 'Book '.$book->name.' updated!');
    }
}
