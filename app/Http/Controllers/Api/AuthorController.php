<?php

namespace App\Http\Controllers\Api;

use App\Author;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    public function index()
    {
        $authors = Author::all();

        return response($authors);
    }

    public function show(Author $author)
    {
        return response($author);
    }

    public function create(Request $request)
    {
        $author = new Author;
        $author->first_name = $request->first_name;
        $author->last_name = $request->last_name;
        $author->save();

        return response($author, 201);
    }

    public function update($idAuthor, Request $request)
    {
        $author = Author::find($idAuthor);

        if (!$author) {
            $message = 'Nu exista autorul cu id '.$idAuthor;

            return response(['message' => $message], 404);
        }

        $author->first_name = $request->first_name;
        $author->last_name = $request->last_name;
        $author->save();

        return response($author);
    }

    public function delete($idAuthor)
    {
        $author = Author::find($idAuthor);

        if (!$author) {
            $message = 'Nu exista autorul cu id '.$idAuthor;

            return response(['message' => $message], 404);
        }

        $author->delete();

        return response([], 200);
    }
}
