<?php

namespace App\Http\Controllers\Api;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();

        return response($books);
    }
    
    public function show($id)
    {
        $book = Book::find($id);

        return response($book);
    }
}
