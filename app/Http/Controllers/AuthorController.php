<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function create()
    {
        return view('authors.create');
    }

    public function __construct()
    {
        $this->middleware('auth')->except([
            'index',
            'show',
        ]);
    }

    public function store(Request $request)
    {
        $author = new Author;
        $author->first_name = $request->first_name;
        $author->last_name = $request->last_name;
        $author->save();

        $message = 'Author '
            .$request->first_name
            .' '
            .$request->last_name
            .' a fost creat.';

        return redirect('/authors')->with('status', $message);
    }

    public function index()
    {
        $authors = Author::paginate(10);

        return view('authors.index', ['authors' => $authors]);
    }

    public function show(Author $author)
    {
        return view('authors.show', ['author' => $author]);
    }

    public function delete(Author $author)
    {
        $author->delete();

        $message = 'Author '
            .$author->first_name
            .' '
            .$author->last_name
            .' a fost sters.';

        return redirect('/authors')->with('status', $message);
    }
}
