<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'name',
        'price',
        'content',
        'page_number',
        'official',
        'added_by',
    ];

    // Accessors:

    /**
     * Cream un "mutator" (acessor)
     * https://laravel.com/docs/5.4/eloquent-mutators#defining-an-accessor
     * 
     * Se apeleaza cu ->page_number
     * Sau cu ->pageNumber
     * 
     * Are prioritate in fata la numele original din baza de date
     */
    public function getPageNumberAttribute()
    {
        // return 333;
        // Cele 2 expresii sunt echivalente.
        // return $this->page_number;
        return $this->attributes['page_number'];
    }

    public function getNumarulDePaginiAttribute()
    {
        return $this->attributes['page_number'];
    }

    public function addedBy()
    {
        // Specificam al doilea parametru drept numele coloanei din baza de date.
        return $this->belongsTo(User::class, 'added_by');
    }
}
