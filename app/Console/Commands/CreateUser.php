<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create:user {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user by email and password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');

        $user = User::where('email', $email)->first();

        $exists = true;

        if (!$user) {
            $user = factory(User::class)->create([
                'email' => $email
            ]);

            $exists = false;
        }

        $user->password = bcrypt($password);
        $user->save();

        if ($exists) {
            $this->comment("Successfully updated found user with password.");
        } else {
            $this->comment("Successfully created new user.");
        }
    }
}
