
# Comenzi folosite pentru a crea proiectul:

## laravel new bookstore-learn
Trebuie sa aveti composer-ul instalat si adaugata linia:
composer global require "laravel/installer".

## asignare de git.

## asignare de virtual host:
C:\xampp\apache\conf\extra\httpd-vhosts.conf
`
<VirtualHost 127.0.0.10:80>
    DocumentRoot C:/xampp/htdocs/bookstore-learn/public
    ServerName bookstore-learn.dev
</VirtualHost>
`
C:\Windows\System32\drivers\etc\hosts
`127.0.0.10   bookstore-learn.dev`
(restart appache)

## php artisan make:model Book --migration --controller

## php artisan make:model Author --migration --controller

## php artisan migrate
Trebuie sa ma asigur ca am editat in fisierul .env constantele ce tin de baza de date, si ca am baza de date creata. (goala)
(in cazul in care avem eroarea asta: `SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too
  long; max key length is 767 bytes`), trebuie editate cele 2 tabele implicite de la laravel (create_users_table si create_password_resets) astfel incat toate stringurile sa aiba o limita de 128 caractere.

## php artisan make:seeder BooksSeeder  
## php artisan make:seeder AuthorsSeeder  
## php artisan db:seed

Oricand intimpinam erori de genul `[ReflectionException]
  Class BooksSeeder does not exist`
  Adaugati comanda `composer dump-autoload` sau pe scurt doar `composer dump`

## php artisan make:controller Api\\BookController
## php artisan make:controller Api\\AuthorController

## php artisan make:auth
## php artisan vendor:publish
## composer require guzzlehttp/guzzle

# Rulam teste:

## vendor/bin/phpunit tests/

# Adaugam consola de debug:

## composer require barryvdh/laravel-debugbar
### 

After updating composer, add the ServiceProvider to the providers array in config/app.php

`Barryvdh\Debugbar\ServiceProvider::class,`